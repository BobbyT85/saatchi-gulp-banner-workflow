/*
Use this to run -
npm install
gulp --p "ft" --rich true

Compile for deploy & zip - 
gulp zip --z "zipName"

Watch -
gulp watch


--------------------------------------------------------------


npm install

gulp start --platform "platform" --rich true --standard false
gulp start --p "platform"

gulp zip --zipName "zipName"
gulp zip --zip "zipName"
gulp zip --z "zipName"

Needs quotes for the string
*/


// Requires
const   archiver                = require("archiver"),
        autoprefixer            = require("autoprefixer"),
        browserSync             = require("browser-sync"),
        chalk                   = require("chalk"),
        cssNano                 = require("cssnano"),
        del                     = require("del"),
        fs                      = require("fs"),
        gulp                    = require("gulp"),
        gulpConcat              = require("gulp-concat"), 
        gulpGit                 = require("gulp-git"),
        gulpPlumber             = require("gulp-plumber"),
        gulpSass                = require("gulp-sass"),
        gulpSourcemaps          = require("gulp-sourcemaps"),
        gulpUglify              = require("gulp-uglify"),
        mergeStream             = require("merge-stream"),
        gulpPostCSS             = require("gulp-postcss"),
        yargs                   = require("yargs").argv;

// Folder paths        
const   srcPath                 = "src/",
        distPath                = "dist/",
        fontsSrc                = "src/fonts/",
        fontsDist               = "dist/fonts/",
        imagesSrc               = "src/images/",
        imagesDist              = "dist/images/",
        jsSrc                   = "src/scripts/",
        jsDist                  = "dist/scripts/",
        sassSrc                 = "src/scss/",
        sassDist                = "dist/styles/",
        videosSrc               = "src/videos/",
        videosDist              = "dist/videos/";

// Watch folders for BrowserSync
const   stylesWatchFolder       = "src/scss/**/*.scss",
        scriptsWatchFolder      = "src/scripts/**/*.js",
        manifestWatch           = "src/*.js",
        htmlWatch               = "src/**/*.html";

// Template repos
const   standardTemplate        = "https://bitbucket.org/saatchisaatchilndimedia/saatchi-banner-exercise-rt",
        doubleclickRich         = "https://bitbucket.org/saatchisaatchilndimedia/saatchi-banner-exercise-rt",
        doubleclickStandard     = "https://bitbucket.org/saatchisaatchilndimedia/saatchi-banner-exercise-rt",
        flashtalkingRich        = "https://bitbucket.org/BobbyT85/structure-test",
        flashtalkingStandard    = "https://bitbucket.org/BobbyT85/structure-test",
        sizmekRich              = "https://bitbucket.org/saatchisaatchilndimedia/saatchi-banner-exercise-rt",
        sizmekStandard          = "https://bitbucket.org/saatchisaatchilndimedia/saatchi-banner-exercise-rt";

// Chalk terminal colours
const   black                   = "#000",
        dodgerBlue              = "#1E90FF",
        indianRed               = "#CD5C5C",
        orange                  = "#EF6C00";
        
let     repo,
        platform, standard, rich, type, zipName;





//************************************************************************************************/
// TASKS *****************************************************************************************/
//************************************************************************************************/

//---------------------------------------------------------------
// CREATE DEPLOY FOLDER FOR FINAL ZIP FILE ----------------------
function createDeploy() {
    return gulp.src("*.*", {read: false})
        .pipe(gulp.dest("deploy"));
}


//---------------------------------------------------------------
// CLEAN DIST FOLDER --------------------------------------------
function cleanDist() {
    console.log(chalk.white.bgHex(indianRed).bold("  :: cleaning dist ::  "));
    return del(["dist/**/*"]);
}


//---------------------------------------------------------------
// CLEAN DEPLOY FOLDER ------------------------------------------
function cleanDeploy() {
    console.log(chalk.white.bgHex(indianRed).bold("  :: cleaning deploy ::  "));
    return del(["deploy/**/*"]);
}


//---------------------------------------------------------------
// COPY WHOLE SRC FOLDER EXCLUDING SCSS AND ITS CONTENTS --------
function copyStructure() {
    // Copy all folders and files in src but exclude the scss folder and its contents
    return gulp.src(["src/**/*", "!src/scss/", "!src/scss/**", "!src/scripts/", "!src/scripts/**"])
        .pipe(gulp.dest("dist"));
}


//---------------------------------------------------------------
// COMPILE AND COPY ---------------------------------------------
function compileCode() {
    // Comment out plugins that aren't needed for CSS
    let plugins = [
        autoprefixer(),
        // cssNano()
    ];

    return mergeStream(
        // SASS - compiles SCSS and creates/updates CSS with sourcemaps
        gulp.src([sassSrc + "*.scss"])
            .pipe(gulpPlumber(function ($error) {
                console.log(chalk.black.bgRed.bold("  :: styles task error ::  "));
                console.log(chalk.black.bgRed.bold("  " + $error + "  "));    
                
                this.emit("end");
            }))

            .pipe(gulpSourcemaps.init({loadMaps: true}))
            .pipe(gulpSass({
                outputStyle: "expanded"
            }).on("error", gulpSass.logError))
            .pipe(gulpPostCSS(plugins))
            .pipe(gulpSourcemaps.write())
            .pipe(gulp.dest(sassDist)),


        // JS - concats and uglifies code into logic.min.js
        gulp.src([jsSrc + "ready.min.js", jsSrc + "vars.min.js", jsSrc + "politeLoad.min.js", jsSrc + "logic.js"])
            
            // Concat - uncomment to enable
            // Left on to prevent HTML linking issues from src HTML
            // Have a big comment header in src JS file to show where the baner logic starts
            .pipe(gulpConcat("logic.js"))
            
            // Uglify - uncomment to enable
            //.pipe(gulpUglify())
            
            .pipe(gulp.dest(jsDist))
    )
}

function compileCodeForDeploy() {
    // Comment out plugins that aren't needed for CSS
    let plugins = [
        autoprefixer(),
        // cssNano()
    ];

    return mergeStream(
        // SASS - compiles SCSS and creates/updates CSS without sourcemaps
        gulp.src([sassSrc + "*.scss"])
            .pipe(gulpPlumber(function ($error) {
                console.log(chalk.white.bgRed.bold("  :: styles task error ::  "));
                console.log(chalk.white.bgRed.bold("  " + $error + "  "));    
                
                this.emit("end");
            }))

            .pipe(gulpSass({
                outputStyle: "expanded"
            }).on("error", gulpSass.logError))
            .pipe(gulpPostCSS(plugins))
            .pipe(gulp.dest(sassDist)),


        // JS - concats and uglifies code into logic.min.js
        gulp.src([jsSrc + "ready.min.js", jsSrc + "vars.min.js", jsSrc + "politeLoad.min.js", jsSrc + "logic.js"])
            
            // Concat - uncomment to enable
            // Left on to prevent HTML linking issues from src HTML
            // Have a big comment header in src JS file to show where the baner logic starts
            .pipe(gulpConcat("logic.js"))
            
            // Uglify - uncomment to enable
            //.pipe(gulpUglify())
            
            .pipe(gulp.dest(jsDist))
    )
}


//---------------------------------------------------------------
// BROWSERSYNC WATCH --------------------------------------------
function browserSyncWatch() {    
    browserSync.init(["dist/styles/*.css", "dist/scripts/*.js", "dist/*.js", "dist/*.html"], {
        server: {
            baseDir: "dist/"
        }
    });

    assignParameters();

    const rebuild = gulp.series(cleanDist, copyStructure, compileCode);
    gulp.watch([stylesWatchFolder, scriptsWatchFolder, manifestWatch, htmlWatch], rebuild);
}


//---------------------------------------------------------------
// ZIP DIST FOLDER AND ADD TO DEPLOY ----------------------------
function zip() {
    assignParameters();

    console.log(chalk.white.bgHex(orange).bold("  :: zipping up dist files for deploy ::  "));
    
    let output = fs.createWriteStream("deploy/" + zipName + ".zip"),
        archive = archiver("zip", {
            zlib: {level: 9} // Sets the compression level.
        });
        
    output.on("close", function() {
        console.log(chalk.white.bgHex(dodgerBlue).bold("  :: created '" + zipName + ".zip' @ " + Math.round(archive.pointer() / 1024) + "kb ::  "));
    });

    output.on("end", function() { console.log(chalk.black.bgYellow.bold("  :: data has been drained ::  ")); });
    
    archive.on("warning", function($err) {
        if ($err.code === "ENOENT") {
            // log warning
        } else {
            // throw error
            throw $err;
        }
    });
    
    archive.on("error", function($err) { throw $err; });    
    archive.pipe(output);    
    archive.directory("dist/", false);

    return archive.finalize();
}





//************************************************************************************************/
// UTILS *****************************************************************************************/
//************************************************************************************************/

//---------------------------------------------------------------
// ASSIGN ANY PARAMETERS TO CORRESPONDING VARIABLES -------------
function assignParameters() {
    console.log(chalk.hex(black).bgYellow.bold("  :: assigning parameters ::  "));

    // PLATFORM PARAMETER
    if (!platform) {
        let p = yargs.platform || yargs.p;
        if (p) p = p.toLowerCase();

        switch (p) {
            case "d": case "dc": case "doubleclick":    platform = "doubleclick";   break;
            case "f": case "ft": case "flashtalking":   platform = "flashtalking";  break;
            case "s": case "sz": case "sizmek":         platform = "sizmek";        break;
        }

        if (platform) console.log(chalk.hex(black).bgYellow.bold("  new parameter:  ", chalk.reset.hex(black).bgGreen("  platform - " + platform + "  ")));
    }

    // RICH BANNER PARAMETER
    if (!rich) {
        rich = yargs.rich;
        if (rich) console.log(chalk.hex(black).bgYellow.bold("  new parameter:  ", chalk.reset.hex(black).bgGreen("  rich - " + rich + "  ")));
    }

    // STANDARD BANNER PARAMETER
    if (!standard) {
        standard = yargs.standard;
        if (standard) console.log(chalk.hex(black).bgYellow.bold("  new parameter:  ", chalk.reset.hex(black).bgGreen("  standard - " + standard + "  ")));
    }

    // TYPE PARAMETER
    if (!type) {
        let t = yargs.type || yargs.t;
        if (t) {
            t = t.toLowerCase();
            t = sortString(t);
            type = t;
            if (type) console.log(chalk.hex(black).bgYellow.bold("  new parameter:  ", chalk.reset.hex(black).bgGreen("  type - " + type + "  ")));
        }
    }

    // ZIP FILE NAME PARAMETER
    let temp = yargs.zip || yargs.zipName || yargs.z;
    if (!zipName || (zipName != temp)) {
        zipName = yargs.zip || yargs.zipName || yargs.z;
        if (zipName) console.log(chalk.hex(black).bgYellow.bold("  new parameter:  ", chalk.reset.hex(black).bgGreen("  zip name - " + zipName + "  ")));
    }
}

function sortString($text) {
    return $text.split('').sort().join('');
}


//---------------------------------------------------------------
// FILTER PARAMETERS TO GET REPO URL ----------------------------
function getTemplate() {
    assignParameters();

    switch (platform) {
        case "dcm":
        case "iab":
            repo = standardTemplate;
            break;

        case "doubleclick":
            if      (rich)      { repo = doubleclickRich }
            else if (standard)  { repo = doubleclickStandard }
            break;

        case "flashtalking":
            if      (rich)      { repo = flashtalkingRich }
            else if (standard)  { repo = flashtalkingStandard }
            break;

        case "sizmek":
            if      (rich)      { repo = sizmekRich; }
            else if (standard)  { repo = sizmekStandard }
            break;
    }

    let templateType;
    switch (type) {
        case "d":   templateType = " dynamic template";                 break;
        case "v":   templateType = " video template";                   break;
        case "dv":  templateType = " dynamic template with video";      break;
        default:    templateType = " standard template";                break;
    }
    console.log(chalk.white.bgRed.bold("  :: getting a " + platform + templateType + " ::  "));
    console.log(chalk.white.bgRed.bold("  :: cloning " + repo + " ::  "));
    
    return gulpGit.clone(repo,
        {args:"src"}, function($err) {
            if ($err) throw $err;
        }
    );
}





//************************************************************************************************/
// ASSIGN & SET TASKS ****************************************************************************/
//************************************************************************************************/
exports.browserSyncWatch = browserSyncWatch;


const   build       = gulp.series(createDeploy, getTemplate, copyStructure, compileCode, browserSyncWatch),
        zipBuild    = gulp.series(cleanDist, copyStructure, compileCodeForDeploy, cleanDeploy, zip),
        watch       = gulp.series(cleanDist, copyStructure, compileCode, browserSyncWatch);

        
gulp.task("default", build);
gulp.task("watch", watch);
gulp.task("zip", zipBuild);