/*!
  * domready (c) Dustin Diaz 2014 - License MIT
  */
!function(e,t){typeof module!="undefined"?module.exports=t():typeof define=="function"&&typeof define.amd=="object"?define(t):this[e]=t()}("domready",function(){var e=[],t,n=typeof document=="object"&&document,r=n&&n.documentElement.doScroll,i="DOMContentLoaded",s=n&&(r?/^loaded|^c/:/^loaded|^i|^c/).test(n.readyState);return!s&&n&&n.addEventListener(i,t=function(){n.removeEventListener(i,t),s=1;while(t=e.shift())t()}),function(t){s?setTimeout(t,0):e.push(t)}})
function elementScrape(){var b,c,d,e,f,g,h,i,j,a=document.getElementsByTagName("*");for(e=0,c=a.length;e<c;e++)if(null!=String(a[e].id))for(g=String(a[e].id),b=document.getElementById(g),f=0,d=reservedWords.length;f<d;f++){if((h=g.toLowerCase())===(i=reservedWords[f].toLowerCase())){console.error(":: VAR ERROR - '"+g+"' USED AS AN ELEMENT ID & NEEDS TO BE CHANGED ::"),syntaxClear=!1;break}j=g.replace(/-/g,"_"),syntaxClear&&(window[j]=b)}return syntaxClear&&console.log(":: element scrape successful - ids that contain hyphens are changed to underscores ::"),syntaxClear}var reservedWords=["abstract","await","else","instanceof","super","boolean","enum","int","switch","break","export","interface","synchronized","byte","extends","let","this","case","false","long","throw","catch","final","native","throws","char","finally","new","transient","class","float","null","true","const","for","package","try","continue","function","private","typeof ","debugger","goto","protected","var","default","if","public","void","delete","implements","return","volatile","do","import","short","while","double","in","static","with","alert","frames","outerHeight","all","frameRate","outerWidth","anchor","function","packages","anchors","getClass","pageXOffset","area","hasOwnProperty","pageYOffset","Array","hidden","parent","assign","history","parseFloat","blur","image","parseInt","button","images","password","checkbox","Infinity","pkcs11","clearInterval","isFinite","plugin","clearTimeout","isNaN","prompt","clientInformation","isPrototypeOf","propertyIsEnum","close","java","prototype","closed","JavaArray","radio","confirm","JavaClass","reset","constructor","JavaObject","screenX","crypto","JavaPackage","screenY","Date","innerHeight","scroll","decodeURI","innerWidth","secure","decodeURIComponent","layer","select","defaultStatus","layers","self","document","length","setInterval","element","link","setTimeout","elements","location","status","embed","Math","String","embeds","mimeTypes","submit","encodeURI","name","taint","encodeURIComponent","NaN","text","escape","navigate","textarea","eval","navigator","top","event","Number","toString","fileUpload","Object","undefined","focus","offscreenBuffering","unescape","form","open","untaint","forms","opener","valueOf","frame","option","window","onbeforeunload","ondragdrop","onkeyup","onmouseover","onblur","onerror","onload","onmouseup","ondragdrop","onfocus","onmousedown","onreset","onclick","onkeydown","onmousemove","onsubmit","oncontextmenu","onkeypress","onmouseout","onunload"],syntaxClear=!0;
function loadScript(a,b){console.log(String(":: LOADER - loading "+a+" ::"));var c=a.split(".").pop();switch(c){case"js":var d=document.createElement("script");d.type="text/javascript",d.readyState?d.onreadystatechange=function(){"loaded"!=d.readyState&&"complete"!=d.readyState||(d.onreadystatechange=null,console.log(String(":: LOADER - successfully loaded "+a+" ::")),b())}:(d.onload=function(){b(),console.log(String(":: LOADER - successfully loaded "+a+" ::"))},d.onerror=function(){console.error(String(":: LOADER ERROR - failed to load "+a+" ::"))}),d.src=a,document.getElementsByTagName("head")[0].appendChild(d);break;case"css":var e=document.createElement("link");e.setAttribute("rel","stylesheet"),e.setAttribute("type","text/css"),e.setAttribute("href",a),document.getElementsByTagName("head")[0].appendChild(e),console.log(String(":: LOADER - successfully loaded "+a+" ::")),b()}}function loadImage(a,b){console.log(String(":: LOADER - loading "+a.url+" ::"));var c=document.getElementById(a.div),d=new Image;null==c&&console.error(String(":: LOADER ERROR - div "+a.div+" doesn't exist ::")),d.onload=function(){c.style.backgroundImage="url('"+d.src+"')",console.log(String(":: LOADER - successfully loaded "+d.src+" ::")),b()},d.onerror=function(){console.error(String(":: LOADER ERROR - failed to load "+d.src+" ::"))},d.src=a.url}function politeLoad(a,b){var f,g,c=a.length,d=0,e=function(){++d===c&&b&&b()};for(f=0;f<c;f++)switch(g=typeof a[f]){case"string":loadScript(a[f],e);break;case"object":loadImage(a[f],e)}}
// --------------------------------------------------------------------------------------------------
// SETUP VARIABLES
var standalone = true,
	bannerHeight = 250,
	bannerWidth = 300,
	tl = new TimelineLite({paused:true});





// --------------------------------------------------------------------------------------------------
// INITIALISE
domready(function() {
	console.log(":: dom ready ::");
	if (elementScrape()) init();
});

function init() {
	setTimeout(setupElements, 500);
}

function setupElements() {	
	// console.log(homeTeamOddsCont);
	document.getElementById('homeTeamOddsTabColor').style.fill = homeTeamOddsTab.innerHTML;

	// console.log(awayTeamOddsCont);
	document.getElementById('awayTeamOddsTabColor').style.fill = awayTeamOddsTab.innerHTML;

	anim();
}

function delayedStart() {
	console.log('Banner Started');
	console.log('standalone: ' + standalone);
	if (standalone) tl.play();
}





function anim() {
	tl.addLabel('start', 0)	
	.set(swoosh, {clip:'rect('+bannerHeight+'px,'+bannerWidth+'px,'+bannerHeight+'px,0px)'})

	tl.to(preloadShape, 0.5, {autoAlpha:0}, 'start')

	 .from(home_team_lrg, 0.5, {x:bannerWidth, ease: Expo.easeInOut}, 'start+=0.5')
	 .from(away_team_lrg, 0.5, {x:bannerWidth, ease: Expo.easeInOut}, 'start+=0.7')
	

	tl.addLabel('frame2', 2.3)
	.from(cta, 0.5, {x:bannerWidth, ease:Expo.easeInOut}, 'frame2')

	.to(home_team_lrg, 0.5, {x:bannerWidth, ease: Expo.easeInOut}, 'frame2')
	.to(away_team_lrg, 0.5, {x:bannerWidth, ease: Expo.easeInOut}, 'frame2+=0.2')

	.from(homeTeamOddsCont, 0.5, {x:10, autoAlpha:0}, 'frame2+=0.7')
	.from(drawOddsCont, 0.5, {autoAlpha:0}, 'frame2+=0.7')
	.from(awayTeamOddsCont, 0.5, {x:-10, autoAlpha:0}, 'frame2+=0.7')

	
	tl.addLabel('frame3', 5)
	.to(swoosh, 0.8, {clip:'rect(0px,'+bannerWidth+'px,'+bannerHeight+'px,0px)', ease:Expo.easeInOut}, 'frame3')
	.set(homeTeamOddsCont, {x:10, autoAlpha:0}, 'frame3+=0.8')
	.set(drawOddsCont, {autoAlpha:0}, 'frame3+=0.8')
	.set(awayTeamOddsCont, {x:-10, autoAlpha:0}, 'frame3+=0.8')

	.from(offer1, 0.5, {x:-bannerWidth, ease: Expo.easeOut}, 'frame3+=0.4')
	.from(offer2, 0.5, {x:-bannerWidth, ease: Expo.easeOut}, 'frame3+=0.7')
	.from(offer3, 0.5, {x:-bannerWidth, ease: Expo.easeOut}, 'frame3+=1.0')


	tl.addLabel('frame4', 7.5)
	.to([offer1, offer2, offer3], 0.5, {autoAlpha:0}, 'frame4')

	.from([main_legal, gamble_logo, gamble_URL] , 0.5, {autoAlpha:0}, 'frame4+=0.5')

	.to([main_legal, gamble_logo, gamble_URL] , 0.5, {autoAlpha:0}, 'frame4+=3.0')

	.to(swoosh, 0.8, {clip:'rect('+bannerHeight+'px,'+bannerWidth+'px,'+bannerHeight+'px,0px)', ease:Expo.easeInOut}, 'frame4+=3.0')


	tl.addLabel('endframe', 10.5)	
	.from(end_A, 0.5, {x:-bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=0.4')
    .from(end_B, 0.5, {x:bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=0.6')
	.from(end_C, 0.5, {x:-bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=0.8')
	   
	.to(end_A, 0.5, {x:-bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=2.0')
    .to(end_B, 0.5, {x:bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=2.2')
    .to(end_C, 0.5, {x:-bannerWidth, autoAlpha:0, ease: Expo.easeOut}, 'endframe+=2.4')
	   
	.to(homeTeamOddsCont, 0.5, {x:0, autoAlpha:1}, 'endframe+=2.9')
    .to(drawOddsCont, 0.5, {autoAlpha:1}, 'endframe+=2.9')
    .to(awayTeamOddsCont, 0.5, {x:0, autoAlpha:1}, 'endframe+=2.9')


	setTimeout(delayedStart, 100);
}





// --------------------------------------------------------------------------------------------------
// EVENT LISTENERS
function addListeners() {
	console.log('[Added] Event Listeners');

	mainContainer.addEventListener('click', exitClickHandler);
}





// --------------------------------------------------------------------------------------------------
// EVENT HANDLING
function exitClickHandler() {
	myFT.on("instantads", function() {
		myFT.applyClickTag(myFT.$("body"), 1);
	});
	console.log('BackgroundExit');
}